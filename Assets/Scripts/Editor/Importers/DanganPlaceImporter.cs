﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using DanganRoomObj;

[InitializeOnLoad]
public class DanganPlaceImporter
{
	private const string MenuPath = "/Importers/Place File";

	[MenuItem( DanganEdit_Globals.DanganEdit_Menu + MenuPath )]
	static void OpenWindow()
	{
		string placePath = EditorUtility.OpenFilePanel( "Browse for place", "", "dat" );
		Import( placePath );
	}

	static void Import(string path)
	{
		GameObject root = GameObject.Find( DanganEdit_Globals.RootObj_Place );

		if ( root == null )
		{
			root = new GameObject( DanganEdit_Globals.RootObj_Place );
		}

		DanganPlace placeData = DanganPlace.ReadFromFile( path );

		foreach ( DanganRoomObject obj in placeData.Objects )
		{
			string objName = $"Object {obj.ID}";
			if ( obj.ObjectType == DanganObjectType.CHAR_STATIC )
			{
				objName = $"Character {obj.ID}";
			}
			if ( obj.ObjectType == DanganObjectType.SPAWN )
			{
				objName = $"Spawn {obj.ID}";
			}
			GameObject newRoomObjGO = new GameObject( objName );

			RoomObj newRoomObj = newRoomObjGO.AddComponent<RoomObj>();

			newRoomObj.ID = obj.ID;
			newRoomObj.ModelID = obj.ModelID;

			newRoomObj.Width = obj.Width;
			newRoomObj.Height = obj.Height;

			if ( !System.Enum.IsDefined( typeof(DanganObjectType), obj.ObjectType ) )
			{
				Debug.LogWarning( $"Imported object {objName} has undefined type {obj.ObjectType}" );
			}

			newRoomObj.ObjectType = obj.ObjectType;
			newRoomObj.Unknown2 = obj.Unknown2;

			Vector3 pos = new Vector3( obj.X, obj.Y, obj.Z );
			Vector3 angles = new Vector3( 0, obj.Rotation, 0 );

			newRoomObj.transform.position = pos;
			newRoomObj.transform.eulerAngles = angles;

			newRoomObj.transform.parent = root.transform;
		}
	}
}
