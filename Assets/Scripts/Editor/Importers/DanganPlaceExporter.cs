﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using DanganRoomObj;

[InitializeOnLoad]
public class DanganPlaceExporter
{
	private const string MenuPath = "/Exporters/Place File";

	[MenuItem( DanganEdit_Globals.DanganEdit_Menu + MenuPath )]
	static void OpenWindow()
	{
		string placePath = EditorUtility.SaveFilePanel( "Save place", "", "bg_xx_place.dat", "dat" );
		Export( placePath );
	}

	static void Export(string path)
	{
		GameObject root = GameObject.Find( DanganEdit_Globals.RootObj_Place );

		if ( root == null )
		{
			EditorUtility.DisplayDialog( "Export failed", $"The scene must contain a GameObject named {DanganEdit_Globals.RootObj_Place} that contains place definitions!", "OK" );
			return;
		}

		DanganPlace placeData = new DanganPlace();

		foreach ( Transform child in root.transform )
		{
			RoomObj obj = child.gameObject.GetComponent<RoomObj>();
			if ( obj == null )
			{
				Debug.LogWarning( $"{DanganEdit_Globals.RootObj_Place} child {child.name} has no RoomObj component!" );
				continue;
			}

			DanganRoomObject newRoomObj = new DanganRoomObject();

			newRoomObj.ID = obj.ID;
			newRoomObj.ModelID = obj.ModelID;

			newRoomObj.Width = obj.Width;
			newRoomObj.Height = obj.Height;

			newRoomObj.ObjectType = obj.ObjectType;
			newRoomObj.Unknown2 = obj.Unknown2;
			
			newRoomObj.X = obj.transform.position.x;
			newRoomObj.Y = obj.transform.position.y;				
			newRoomObj.Z = obj.transform.position.z;

			newRoomObj.Rotation = obj.transform.eulerAngles.y;

			placeData.Objects.Add( newRoomObj );
		}
		Debug.Log( $"Exporting {placeData.Objects.Count} {(placeData.Objects.Count == 1 ? "object" : "objects")}" );
		placeData.WriteToFile( path );
	}
}
