﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomObj : MonoBehaviour
{
	public int ID;
	public int ModelID;

	public float Width;
	public float Height;

	public DanganRoomObj.DanganObjectType ObjectType;
	public int Unknown2;


	const float charaHeight = 240;
	const float charaWidth = 120;
	static Color charaColour = Color.green;
	private void OnDrawGizmos()
	{
		Color col = Color.red;
		float h = Height;
		float w = Width;
		float d = 0; //Depth
		Vector3 offset = Vector3.zero;

		if ( ObjectType == DanganRoomObj.DanganObjectType.CHAR_STATIC )
		{
			col = charaColour;
			w = charaWidth;
			h = charaHeight;
			offset = new Vector3( 0, h/2, 0 ); //Move it up so the wire cube aligns with the floor
		}

		Gizmos.matrix = transform.localToWorldMatrix;
		Gizmos.color = col;
		Gizmos.DrawWireCube( Vector3.zero + offset, new Vector3( w, h, 1 ) );
	}
}
